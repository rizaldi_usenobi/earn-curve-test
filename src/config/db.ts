import { Sequelize, Dialect } from "sequelize";
import dotenv from "dotenv";

dotenv.config();

const dialect = process.env.DB_DIALECT as Dialect;

if (typeof dialect !== "string") {
  throw new Error("DB_DIALECT must be defined.");
}

if (typeof process.env.DB_NAME !== "string") {
  throw new Error("DB_NAME must be defined.");
}

if (typeof process.env.DB_USERNAME !== "string") {
  throw new Error("DB_USERNAME must be defined.");
}

if (typeof process.env.DB_PASSWORD !== "string") {
  throw new Error("DB_PASSWORD must be defined.");
}

if (typeof process.env.DB_HOST !== "string") {
  throw new Error("DB_HOST must be defined.");
}

if (typeof process.env.DB_PORT !== "string") {
  throw new Error("DB_PORT must be defined.");
}

export const db = new Sequelize(
  process.env.DB_NAME,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT),
    dialect: dialect,
    timezone: "+07:00",
    sync: {
      alter: true,
      force: true,
    },
    dialectOptions: {
      useUTC: false,
    },
  }
);
