import { DataTypes, Model, Optional } from "sequelize";
import { db } from "../config/db";

const PROTECTED_ATTRIBUTES: string[] = [];

export interface CurveBalanceAttributes {
  id: string;
  address: string;
  total_lp_token: number;
  total_eth: number;
  total_steth: number;
  total_admin_balance_eth: number;
  total_admin_balance_steth: number;
  our_lp_token: number;
  our_eth: number;
  our_steth: number;
  our_admin_balance_eth: number;
  our_admin_balance_steth: number;
  our_ldo: number;
  price_eth_to_usd: number;
  price_steth_to_usd: number;
  virtual_price: number;
  our_admin_balance_eth_in_usd: number;
  our_admin_balance_steth_in_usd: number;
  createdAt: Date;
  updatedAt: Date;
}

interface CurveBalanceCreationAttributes
  extends Optional<CurveBalanceAttributes, "id" | "createdAt" | "updatedAt"> {}

class CurveBalance
  extends Model<CurveBalanceAttributes, CurveBalanceCreationAttributes>
  implements CurveBalanceAttributes
{
  toJSON() {
    // hide protected fields
    let attributes = Object.assign({}, this.get());
    for (let a of PROTECTED_ATTRIBUTES) {
      // TODO: Get the solution of this
      // @ts-ignore
      delete attributes[a];
    }
    return attributes;
  }

  public id!: string;
  public address!: string;
  public total_lp_token!: number;
  public total_eth!: number;
  public total_steth!: number;
  public total_admin_balance_eth!: number;
  public total_admin_balance_steth!: number;
  public our_lp_token!: number;
  public our_eth!: number;
  public our_steth!: number;
  public our_admin_balance_eth!: number;
  public our_admin_balance_steth!: number;
  public our_ldo!: number;
  public price_eth_to_usd!: number;
  public price_steth_to_usd!: number;
  public virtual_price!: number;
  public our_admin_balance_eth_in_usd!: number;
  public our_admin_balance_steth_in_usd!: number;

  // timestamps!
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

CurveBalance.init(
  {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    address: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    total_lp_token: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    total_eth: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    total_steth: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    total_admin_balance_eth: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    total_admin_balance_steth: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    our_lp_token: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    our_eth: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    our_steth: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    our_admin_balance_eth: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    our_admin_balance_steth: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    our_ldo: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    price_eth_to_usd: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    price_steth_to_usd: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    virtual_price: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    our_admin_balance_eth_in_usd: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    our_admin_balance_steth_in_usd: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
  },
  {
    tableName: "curve_balances",
    sequelize: db,
  }
);

// HOOKS

// ASSOCIATIONS
export { CurveBalance };
