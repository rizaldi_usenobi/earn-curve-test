import cron from "node-cron";
import { check_balance_function } from "./check_balance_function";

export const check_balances = () => {
  cron.schedule("0 */5 * * * *", async () => {
    console.log("==========================");
    console.log("Start Checking Balances");

    await check_balance_function();

    console.log("Finished Checking Balances");
    console.log("==========================");
  });
};
