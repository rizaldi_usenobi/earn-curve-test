import { ethers } from "ethers";
import dotenv from "dotenv";
import steth_liquidity_gauge_abi from "../abi/steth_liquidity_gauge_abi";
import curve_eth_steth_stableswap_abi from "../abi/curve_eth_steth_stableswap_abi";
import curve_lp_token_abi from "../abi/curve_lp_token_abi";
import { CurveBalance } from "../models/curve_balance";
import ldo_token_abi from "../abi/ldo_token_abi";
import axios from "axios";

export const check_balance_function = async () => {
  dotenv.config();

  const etherscan_api_key = process.env.ETHERSCAN_API_KEY;
  const curve_user_address = process.env.CURVE_USER_ADDRESS;

  if (typeof curve_user_address !== "string") {
    throw new Error("CURVE_USER_ADDRESS is not set!");
  }

  const provider = new ethers.providers.EtherscanProvider(
    "homestead",
    etherscan_api_key
  );

  // LIQUIDITY GAUGE CONTRACT
  const steth_liquidity_gauge_address =
    "0x182B723a58739a9c974cFDB385ceaDb237453c28";
  const steth_liquidity_gauge_contract = new ethers.Contract(
    steth_liquidity_gauge_address,
    steth_liquidity_gauge_abi,
    provider
  );

  // LIQUIDITY POOL CONTRACT
  const eth_steth_stableswap_address =
    "0xDC24316b9AE028F1497c275EB9192a3Ea0f67022";
  const steth_liquidity_pool_contract = new ethers.Contract(
    eth_steth_stableswap_address,
    curve_eth_steth_stableswap_abi,
    provider
  );

  // CURVE LP TOKEN CONTRACT
  const curve_lp_token_address = "0x06325440D014e39736583c165C2963BA99fAf14E";
  const curve_lp_token_contract = new ethers.Contract(
    curve_lp_token_address,
    curve_lp_token_abi,
    provider
  );

  // LDO TOKEN CONTRACT
  const ldo_contract_address = "0x5a98fcbea516cf06857215779fd812ca3bef1b32";
  const ldo_contract = new ethers.Contract(
    ldo_contract_address,
    ldo_token_abi,
    provider
  );

  const our_lp_token =
    await steth_liquidity_gauge_contract.callStatic.balanceOf(
      curve_user_address
    );
  const our_lp_token_float = parseFloat(ethers.utils.formatEther(our_lp_token));

  const lp_token_total_supply =
    await curve_lp_token_contract.callStatic.totalSupply();
  const float_lp_token_total_supply = parseFloat(
    ethers.utils.formatEther(lp_token_total_supply)
  );

  const total_balance_eth =
    await steth_liquidity_pool_contract.callStatic.balances(0);
  const total_balance_steth =
    await steth_liquidity_pool_contract.callStatic.balances(1);
  const total_balance_eth_float = parseFloat(
    ethers.utils.formatEther(total_balance_eth)
  );
  const total_balance_steth_float = parseFloat(
    ethers.utils.formatEther(total_balance_steth)
  );

  const total_admin_balance_eth =
    await steth_liquidity_pool_contract.callStatic.admin_balances(0);
  const total_admin_balance_steth =
    await steth_liquidity_pool_contract.callStatic.admin_balances(1);
  const total_admin_balance_eth_float = parseFloat(
    ethers.utils.formatEther(total_admin_balance_eth)
  );
  const total_admin_balance_steth_float = parseFloat(
    ethers.utils.formatEther(total_admin_balance_steth)
  );

  const our_balance_eth =
    (total_balance_eth_float * our_lp_token_float) /
    float_lp_token_total_supply;
  const our_balance_steth =
    (total_balance_steth_float * our_lp_token_float) /
    float_lp_token_total_supply;

  const our_admin_balance_eth =
    (total_admin_balance_eth_float * our_lp_token_float) /
    float_lp_token_total_supply;
  const our_admin_balance_steth =
    (total_admin_balance_steth_float * our_lp_token_float) /
    float_lp_token_total_supply;

  const virtual_price =
    await steth_liquidity_pool_contract.callStatic.get_virtual_price();
  const virtual_price_float = parseFloat(
    ethers.utils.formatEther(virtual_price)
  );

  // const our_ldo = await ldo_contract.callStatic.balanceOf(curve_user_address);

  // const our_ldo_float = parseFloat(ethers.utils.formatEther(our_ldo));

  const ldo_token_address =
    await steth_liquidity_gauge_contract.callStatic.reward_tokens(0);
  const our_ldo =
    await steth_liquidity_gauge_contract.callStatic.claimable_reward(
      curve_user_address,
      ldo_token_address
    );
  const our_ldo_float = parseFloat(ethers.utils.formatEther(our_ldo));

  const { data: prices } = await axios.get(
    `https://api.coingecko.com/api/v3/simple/price?ids=ethereum%2Cstaked-ether%2Cldo&vs_currencies=usd&precision=18
    `
  );

  const price_eth_to_usd = prices.ethereum.usd;
  const price_steth_to_usd = prices["staked-ether"].usd;

  await CurveBalance.create({
    address: curve_user_address,
    total_lp_token: float_lp_token_total_supply,
    total_eth: total_balance_eth_float,
    total_steth: total_balance_steth_float,
    total_admin_balance_eth: total_admin_balance_eth_float,
    total_admin_balance_steth: total_admin_balance_steth_float,
    our_lp_token: our_lp_token_float,
    our_eth: our_balance_eth,
    our_steth: our_balance_steth,
    our_admin_balance_eth: our_admin_balance_eth,
    our_admin_balance_steth: our_admin_balance_steth,
    our_ldo: our_ldo_float,
    price_eth_to_usd: price_eth_to_usd,
    price_steth_to_usd: price_steth_to_usd,
    virtual_price: virtual_price_float,
    our_admin_balance_eth_in_usd: our_admin_balance_eth * price_eth_to_usd,
    our_admin_balance_steth_in_usd:
      our_admin_balance_steth * price_steth_to_usd,
  });
};
