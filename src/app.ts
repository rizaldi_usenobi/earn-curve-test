import express, { Request, Response } from "express";
import "express-async-errors";
import cors from "cors";
import dotenv from "dotenv";

import { errorHandler } from "./middlewares/error-handler";

import { NotFoundError } from "./errors/not-found-error";
import { check_balances } from "./scheduler/check_balances";
import { advancedResults } from "./middlewares/advanced-result";
import { CurveBalance } from "./models/curve_balance";
import { db } from "./config/db";
import { check_balance_function } from "./scheduler/check_balance_function";

dotenv.config();

const app = express();

// Set Middlewares
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors({ origin: true }));

// Set Routes
app.get(
  "/api/curve-balances",
  advancedResults(CurveBalance),
  (_req: Request, res: Response) => res.json(res.advancedResults)
);
app.get("/api/db/wipe", async (_req: Request, res: Response) => {
  await db.sync();
  res.json({ message: "Success Wiped Database!" });
});

app.get("/api/snapshot-curve-balance", async (_req: Request, res: Response) => {
  await check_balance_function();
  res.status(200).json({ message: "Success Snapshot Curve Balance" });
});

// Set Route Not Found Error
app.all("*", async (_req: Request, _res: Response) => {
  throw new NotFoundError();
});

// Set Custom Error Handler
app.use(errorHandler);

// Schedulers
check_balances();

export { app };
